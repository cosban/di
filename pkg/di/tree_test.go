package di

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/cosban/di/v2/pkg/model"
)

func TestEmptyProvider(t *testing.T) {
	type given struct {
		Providers []model.Provider
	}
	type expected struct {
		Tree treeNode
	}
	type test struct {
		Name     string
		Given    given
		Expected expected
	}
	for _, scenario := range []test{
		{
			Name: "empty tree",
			Given: given{
				Providers: []model.Provider{},
			},
			Expected: expected{
				Tree: emptyproviderTree(),
			},
		}, {
			Name: "single node",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool")),
				},
			},
			Expected: expected{
				Tree: emptyproviderTree().withDependency(
					model.Identifier{
						Name: "a",
						Type: "bool",
					},
					testproviderTree("a", "bool"),
				),
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			t.Parallel()
			actual := toTree(scenario.Given.Providers)
			assert.Equal(t, scenario.Expected.Tree, actual)
		})
	}
}

func testproviderTree(name, kind string) treeNode {
	return emptyproviderTree().withidentifier(model.Identifier{Name: name, Type: kind})
}

func emptyproviderTree() treeNode {
	return newTreeNode(model.Identifier{})
}

func (p treeNode) withidentifier(ident model.Identifier) treeNode {
	return treeNode{
		Identifier:   ident,
		dependencies: p.dependencies,
		prerequisite: p.prerequisite,
	}
}

func (p treeNode) withDependency(ident model.Identifier, value treeNode) treeNode {
	r := treeNode{
		Identifier:   p.Identifier,
		dependencies: p.dependencies,
		prerequisite: p.prerequisite,
	}
	r.dependencies[ident] = value
	return r
}

func (p treeNode) withPrerequisite(ident model.Identifier) treeNode {
	return treeNode{
		Identifier:   p.Identifier,
		dependencies: p.dependencies,
		prerequisite: append(p.prerequisite, ident),
	}
}
