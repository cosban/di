package buffer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormatBuffer_Write(t *testing.T) {
	type given struct {
		Template string
		Args     []any
	}
	type expected struct {
		String string
		Err    error
	}
	type test struct {
		Name     string
		Given    given
		Expected expected
	}
	for _, scenario := range []test{
		{
			Name: "empty",
			Given: given{
				Template: "",
				Args:     []any{},
			},
			Expected: expected{
				String: "\n",
				Err:    nil,
			},
		}, {
			Name: "single",
			Given: given{
				Template: "%s",
				Args:     []any{"a"},
			},
			Expected: expected{
				String: "a\n",
				Err:    nil,
			},
		}, {
			Name: "multiple",
			Given: given{
				Template: "%s %s",
				Args:     []any{"a", "b"},
			},
			Expected: expected{
				String: "a b\n",
				Err:    nil,
			},
		}, {
			Name: "no vars",
			Given: given{
				Template: "a b",
				Args:     []any{},
			},
			Expected: expected{
				String: "a b\n",
				Err:    nil,
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			t.Parallel()
			b := &FormatBuffer{}
			_, err := b.Write(scenario.Given.Template, scenario.Given.Args...)
			assert.Equal(t, scenario.Expected.Err, err)
			assert.Equal(t, scenario.Expected.String, b.String())
		})
	}
}
