module gitlab.com/cosban/di/v2

go 1.21

require (
	github.com/spf13/cobra v1.8.0
	github.com/stretchr/testify v1.8.4
	gitlab.com/cosban/di v0.0.0-20230721011949-8e9da1056cd7
	golang.org/x/text v0.14.0
	golang.org/x/tools v0.16.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/mod v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
