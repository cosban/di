package examples

import "bytes"

type Printer interface {
	Print() string
}

type DoublePrinter struct {
	Left, Right Printer
}

func (d *DoublePrinter) Print() string {
	return d.Left.Print() + d.Right.Print()
}

type BufferPrinter struct {
	Buffer *bytes.Buffer
}

func (w BufferPrinter) Print() string {
	return w.Buffer.String()
}

type GenericPrinter[T any] struct {
	Name string
}

type PrinterEvaluator[T any] struct {
	Printer GenericPrinter[T]
}
