package di

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/cosban/di/v2/pkg/model"
)

func TestCyclicalErrorCanBeAsserted(t *testing.T) {
	err := CyclicalError(model.Identifier{}, model.Identifier{})
	assert.ErrorIs(t, err, ErrCyclicalDependency)
}

func TestMissingErrorCanBeAsserted(t *testing.T) {
	err := MissingError(model.Provider{}, model.Identifier{})
	assert.ErrorIs(t, err, ErrMissingDependency)
}

func TestDependencyValidation(t *testing.T) {
	type given struct {
		Providers []model.Provider
	}
	type expected struct {
		Err error
	}
	type test struct {
		Name     string
		Given    given
		Expected expected
	}
	for _, scenario := range []test{
		{
			Name: "missing dependency",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool").WithRequirement("b", "bool")),
				},
			},
			Expected: expected{
				Err: ErrMissingDependency,
			},
		}, {
			Name: "loose cyclical dependency",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool").WithRequirement("b", "bool")),
					*(testprovider("b", "bool").WithRequirement("c", "bool")),
					*(testprovider("c", "bool").WithRequirement("a", "bool")),
				},
			},
			Expected: expected{
				Err: ErrCyclicalDependency,
			},
		}, {
			Name: "tight cyclical dependency",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool").WithRequirement("b", "bool")),
					*(testprovider("b", "bool").WithRequirement("a", "bool")),
				},
			},
			Expected: expected{
				Err: ErrCyclicalDependency,
			},
		}, {
			Name: "depends on self",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool").WithRequirement("a", "bool")),
				},
			},
			Expected: expected{
				Err: ErrCyclicalDependency,
			},
		}, {
			Name: "valid dependency tree",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool").WithRequirement("b", "bool")),
					*(testprovider("b", "bool").WithRequirement("c", "bool")),
					*(testprovider("c", "bool")),
				},
			},
			Expected: expected{
				Err: nil,
			},
		}, {
			Name: "no dependencies",
			Given: given{
				Providers: []model.Provider{
					*(testprovider("a", "bool")),
				},
			},
			Expected: expected{
				Err: nil,
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			err := validateDependencyTree(scenario.Given.Providers)
			assert.ErrorIs(t, err, scenario.Expected.Err)
		})
	}
}

func testprovider(name, kind string) *model.Provider {
	return &model.Provider{
		Identifier:   model.Identifier{Name: name, Type: kind},
		Requirements: make([]model.Identifier, 0),
	}
}
