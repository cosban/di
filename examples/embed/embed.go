//go:generate di generate --output=embed.gen.go --source=embed.go
package embed

import (
	"fmt"

	"gitlab.com/cosban/di/v2/examples/simple"
)

// @component
func SimpleComponent() *simple.SimpleComponent {
	return simple.GetSimpleComponent()
}

func Calcium() string {
	return fmt.Sprintf("%s", "sodium")
}
