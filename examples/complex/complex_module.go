//go:generate di generate --output=complex_module.gen.go --source=complex_module.go
package complex

import (
	"bytes"

	"gitlab.com/cosban/di/examples"
)

func AminoPrinter(leftPrinter examples.Printer, rightPrinter examples.Printer) examples.Printer {
	return &examples.DoublePrinter{Left: leftPrinter, Right: rightPrinter}
}

func LeftPrinter(left *bytes.Buffer) examples.Printer {
	return &examples.BufferPrinter{
		Buffer: left,
	}
}

func RightPrinter(right *bytes.Buffer) examples.Printer {
	return &examples.BufferPrinter{
		Buffer: right,
	}
}

func Left() *bytes.Buffer {
	return bytes.NewBufferString("left")
}

func Right(prefix string) *bytes.Buffer {
	return bytes.NewBufferString(prefix + "right")
}

func Prefix() string {
	return " "
}
