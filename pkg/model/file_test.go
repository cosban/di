package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFile(t *testing.T) {
	actual := NewFile("source.go", "model")
	assert.NotNil(t, actual)

	// test for panics due to nil maps
	actual.Providers.Add(Provider{})
	actual.Components[Identifier{}] = &EmbeddedComponent{}
}

func TestMergeProviders(t *testing.T) {
	type given struct {
		Providers  []Provider
		Components []*EmbeddedComponent
	}
	type expected struct {
		Providers []Provider
		Error     error
	}
	type test struct {
		Name     string
		Given    given
		Expected expected
	}
	for _, scenario := range []test{
		{
			Name: "no components",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
				},
			},
			Expected: expected{
				Providers: []Provider{
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
				},
			},
		}, {
			Name: "no overlap",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
				},
				Components: []*EmbeddedComponent{
					{
						Identifier: Identifier{Name: "Component", Type: "*component.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "boolProvider", Type: "bool"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					},
				},
			},
			Expected: expected{
				Providers: []Provider{
					{Identifier: Identifier{Name: "boolProvider", Type: "bool"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "floatProvider", Type: "float"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
				},
			},
		}, {
			Name: "overlap",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
				},
				Components: []*EmbeddedComponent{
					{
						Identifier: Identifier{Name: "Component", Type: "*component.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					},
				},
			},
			Expected: expected{
				Providers: []Provider{
					{Identifier: Identifier{Name: "floatProvider", Type: "float"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
				},
			},
		}, {
			Name: "multiple components without collision",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
				},
				Components: []*EmbeddedComponent{
					{
						Identifier: Identifier{Name: "Component", Type: "*first.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "boolProvider", Type: "bool"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					}, {
						Identifier: Identifier{Name: "Component", Type: "*second.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
							{Identifier: Identifier{Name: "sliceProvider", Type: "[]string"}},
						}),
					},
				},
			},
			Expected: expected{
				Providers: []Provider{
					{Identifier: Identifier{Name: "boolProvider", Type: "bool"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "floatProvider", Type: "float"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "sliceProvider", Type: "[]string"}, Package: "embedded_2"},
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
				},
			},
		}, {
			Name: "multiple components with non-overridden collision",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
				},
				Components: []*EmbeddedComponent{
					{
						Identifier: Identifier{Name: "Component", Type: "*first.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "boolProvider", Type: "bool"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					}, {
						Identifier: Identifier{Name: "Component", Type: "*second.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					},
				},
			},
			Expected: expected{
				Error: ErrDuplicateProviderIdentifiers,
			},
		}, {
			Name: "multiple components with overridden collision",
			Given: given{
				Providers: []Provider{
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
				},
				Components: []*EmbeddedComponent{
					{
						Identifier: Identifier{Name: "Component", Type: "*first.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "boolProvider", Type: "bool"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					}, {
						Identifier: Identifier{Name: "Component", Type: "*second.Component"},
						Providers: FromProviders([]Provider{
							{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
							{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
						}),
					},
				},
			},
			Expected: expected{
				Providers: []Provider{
					{Identifier: Identifier{Name: "boolProvider", Type: "bool"}, Package: "embedded_1"},
					{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
					{Identifier: Identifier{Name: "intProvider", Type: "int"}},
					{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
				},
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			file := NewFile("", "")
			file.Providers = FromProviders(scenario.Given.Providers)
			file.Components = mapComponents(scenario.Given.Components)
			actual, err := file.MergeProviders()
			assert.ErrorIs(t, err, scenario.Expected.Error)
			if scenario.Expected.Error == nil {
				assert.Equal(t, scenario.Expected.Providers, actual.Slice())
			} else {
				assert.Nil(t, actual)
			}
		})
	}
}

func mapComponents(components []*EmbeddedComponent) map[Identifier]*EmbeddedComponent {
	mapped := make(map[Identifier]*EmbeddedComponent)
	for _, component := range components {
		mapped[component.Identifier] = component
	}
	return mapped
}

func TestProviderCollection_GetSortedIdentifiers(t *testing.T) {
	providers := FromProviders([]Provider{
		{Identifier: Identifier{Name: "zebraProvider", Type: "zebra"}},
		{Identifier: Identifier{Name: "stringProvider", Type: "string"}},
		{Identifier: Identifier{Name: "intProvider", Type: "int"}},
		{Identifier: Identifier{Name: "floatProvider", Type: "float"}},
	})
	actual := providers.GetSortedIdentifiers()
	assert.Equal(t, []Identifier{
		{Type: "float", Name: "floatProvider"},
		{Type: "int", Name: "intProvider"},
		{Type: "string", Name: "stringProvider"},
		{Type: "zebra", Name: "zebraProvider"},
	}, actual)
}
