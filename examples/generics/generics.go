//go:generate di generate --output=generics.gen.go --source=generics.go
package generics

import (
	"gitlab.com/cosban/di/examples"
)

func Name() string {
	return "Rachel"
}

func GenericPrinter(name string) examples.GenericPrinter[int] {
	return examples.GenericPrinter[int]{
		Name: name,
	}
}

func PrinterEvaluator(genericPrinter examples.GenericPrinter[int]) examples.PrinterEvaluator[int] {
	return examples.PrinterEvaluator[int]{
		Printer: genericPrinter,
	}
}
