//go:generate di generate --output=singletons_module.gen.go --source=singletons_module.go
package singletons

import (
	"bytes"

	"gitlab.com/cosban/di/examples"
)

// @singleton
func Printer(buffer *bytes.Buffer) examples.Printer {
	return &examples.BufferPrinter{
		Buffer: buffer,
	}
}

// @singleton
func Buffer() *bytes.Buffer {
	return bytes.NewBufferString("")
}
