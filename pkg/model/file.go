package model

import (
	"errors"
	"fmt"
	"slices"
	"strings"
)

// File is the model of a parsed file. It contains the imports used within the original file as well as a slice of all
// direct providers defined within the file.
type File struct {
	Source     string
	Package    string
	Imports    []string
	Providers  *ProviderCollection
	Components map[Identifier]*EmbeddedComponent
}

func NewFile(source, pkg string) *File {
	return &File{
		Source:  source,
		Package: pkg,
		Providers: &ProviderCollection{
			slice:   make([]Provider, 0),
			byIdent: make(map[Identifier]Provider),
		},
		Components: make(map[Identifier]*EmbeddedComponent),
	}
}

var ErrDuplicateProviderIdentifiers = errors.New("duplicate provider identifiers found which were not overridden")

func (f File) MergeProviders() (*ProviderCollection, error) {
	providerSet := make(map[Identifier]Provider)
	duplicates := make(map[Identifier]struct{})
	i := 1

	components := make([]*EmbeddedComponent, 0, len(f.Components))
	for _, component := range f.Components {
		components = append(components, component)
	}
	slices.SortFunc(components, func(a, b *EmbeddedComponent) int {
		return strings.Compare(a.Identifier.String(), b.Identifier.String())
	})

	for _, component := range components {
		component.PackageAlias = fmt.Sprintf("embedded_%d", i)
		i++
		for _, provider := range component.Providers.Slice() {
			if _, ok := providerSet[provider.Identifier]; ok {
				duplicates[provider.Identifier] = struct{}{}
			}
			providerSet[provider.Identifier] = Provider{
				Identifier:   provider.Identifier,
				Package:      component.PackageAlias,
				IsSingleton:  provider.IsSingleton,
				IsComponent:  provider.IsComponent,
				Requirements: provider.Requirements,
			}
		}
	}
	for _, provider := range f.Providers.Slice() {
		if _, ok := providerSet[provider.Identifier]; ok {
			delete(duplicates, provider.Identifier)
		}
		providerSet[provider.Identifier] = provider
	}
	if len(duplicates) > 0 {
		return nil, ErrDuplicateProviderIdentifiers
	}
	providers := make([]Provider, 0, len(providerSet))
	for _, provider := range providerSet {
		providers = append(providers, provider)
	}
	slices.SortFunc(providers, func(a, b Provider) int {
		return strings.Compare(a.Identifier.String(), b.Identifier.String())
	})
	return FromProviders(providers), nil
}

type ProviderCollection struct {
	slice   []Provider
	byIdent map[Identifier]Provider
}

func FromProviders(providers []Provider) *ProviderCollection {
	collection := &ProviderCollection{
		slice:   make([]Provider, 0, len(providers)),
		byIdent: make(map[Identifier]Provider),
	}
	for _, provider := range providers {
		collection.Add(provider)
	}
	return collection
}

func (p *ProviderCollection) Add(provider Provider) {
	if _, ok := p.byIdent[provider.Identifier]; !ok {
		p.slice = append(p.slice, provider)
		p.byIdent[provider.Identifier] = provider
	}
}

func (p *ProviderCollection) GetSortedIdentifiers() []Identifier {
	identifiers := make([]Identifier, 0, len(p.byIdent))
	for _, provider := range p.Slice() {
		identifiers = append(identifiers, provider.Identifier)
	}
	slices.SortFunc(identifiers, func(a, b Identifier) int {
		return strings.Compare(a.String(), b.String())
	})
	return identifiers
}

func (p *ProviderCollection) Slice() []Provider {
	return p.slice
}

func (p *ProviderCollection) ByIdentifier(ident Identifier) Provider {
	return p.byIdent[ident]
}
