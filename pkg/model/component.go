package model

type EmbeddedComponent struct {
	Identifier

	PackageAlias string
	ImportPath   string

	Imports    []string
	Functions  map[Identifier]struct{}
	Singletons []Identifier
	Providers  *ProviderCollection
}

func NewEmbeddedComponent(identifier Identifier) *EmbeddedComponent {
	return &EmbeddedComponent{
		Identifier: identifier,
		Imports:    make([]string, 0),
		Functions:  make(map[Identifier]struct{}),
		Singletons: make([]Identifier, 0),
		Providers: &ProviderCollection{
			slice:   make([]Provider, 0),
			byIdent: make(map[Identifier]Provider),
		},
	}
}
