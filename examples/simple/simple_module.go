//go:generate di generate --output=simple_module.gen.go --source=simple_module.go
package simple

import (
	"bytes"
	"fmt"

	"gitlab.com/cosban/di/examples"
)

func Calcium() string {
	return fmt.Sprintf("%s", "calcium")
}

func CalciumBuffer(calcium string) *bytes.Buffer {
	return bytes.NewBufferString(calcium)
}

func BufferPrinter(calciumBuffer *bytes.Buffer) examples.Printer {
	return &examples.BufferPrinter{
		Buffer: calciumBuffer,
	}
}
