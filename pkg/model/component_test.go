package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewEmbeddedComponent(t *testing.T) {
	type given struct {
		identifier Identifier
	}
	type expected struct {
		Component *EmbeddedComponent
	}
	for _, scenario := range []struct {
		Name     string
		Given    given
		Expected expected
	}{
		{
			Name: "empty",
			Given: given{
				identifier: Identifier{},
			},
			Expected: expected{
				Component: &EmbeddedComponent{
					Identifier: Identifier{},
					Imports:    make([]string, 0),
					Functions:  make(map[Identifier]struct{}),
					Singletons: make([]Identifier, 0),
					Providers: &ProviderCollection{
						slice:   make([]Provider, 0),
						byIdent: make(map[Identifier]Provider),
					},
				},
			},
		}, {
			Name: "with identifier",
			Given: given{
				identifier: Identifier{Name: "Component", Type: "*component.Component"},
			},
			Expected: expected{
				Component: &EmbeddedComponent{
					Identifier: Identifier{Name: "Component", Type: "*component.Component"},
					Imports:    make([]string, 0),
					Functions:  make(map[Identifier]struct{}),
					Singletons: make([]Identifier, 0),
					Providers: &ProviderCollection{
						slice:   make([]Provider, 0),
						byIdent: make(map[Identifier]Provider),
					},
				},
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			actual := NewEmbeddedComponent(scenario.Given.identifier)
			assert.Equal(t, scenario.Expected.Component, actual)
		})
	}
}
