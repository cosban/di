//go:generate di generate --output=missing.gen.go --source=missing.go
package incomplete

import "gitlab.com/cosban/di/examples"

func DeadPrinter(missing string) examples.Printer {
	return nil
}
