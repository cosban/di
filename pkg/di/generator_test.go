package di

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerator(t *testing.T) {
	type given struct {
		Source string
	}
	type expected struct {
		src   string
		Error error
	}
	scenarios := []struct {
		Name     string
		Given    given
		Expected expected
	}{
		{
			Name: "complex",
			Given: given{
				Source: "../../examples/complex/complex_module.go",
			},
			Expected: expected{
				src: "package complex\n\nimport (\n\"bytes\"\n\"gitlab.com/cosban/di/examples\"\n)\nvar singleton *ComplexComponent\nfunc GetComplexComponent() *ComplexComponent {\nif singleton == nil {\nsingleton = &ComplexComponent{}\n}\nreturn singleton\n}\ntype ComplexComponent struct {\n}\nfunc (c *ComplexComponent) Left() *bytes.Buffer {\nreturn Left(\n)\n}\nfunc (c *ComplexComponent) Prefix() string {\nreturn Prefix(\n)\n}\nfunc (c *ComplexComponent) LeftPrinter() examples.Printer {\nreturn LeftPrinter(\nc.Left(),\n)\n}\nfunc (c *ComplexComponent) Right() *bytes.Buffer {\nreturn Right(\nc.Prefix(),\n)\n}\nfunc (c *ComplexComponent) AminoPrinter() examples.Printer {\nreturn AminoPrinter(\nc.LeftPrinter(),\nc.RightPrinter(),\n)\n}\nfunc (c *ComplexComponent) RightPrinter() examples.Printer {\nreturn RightPrinter(\nc.Right(),\n)\n}\n",
			},
		}, {
			Name: "embed",
			Given: given{
				Source: "../../examples/embed/embed.go",
			},
			Expected: expected{
				src: "package embed\n\nimport (\n\"fmt\"\n\"gitlab.com/cosban/di/v2/examples/simple\"\n\"bytes\"\n\"fmt\"\n\"gitlab.com/cosban/di/examples\"\nembedded_1 \"gitlab.com/cosban/di/v2/examples/simple\"\n)\nvar singleton *EmbedComponent\nfunc GetEmbedComponent() *EmbedComponent {\nif singleton == nil {\nsingleton = &EmbedComponent{}\n}\nreturn singleton\n}\ntype EmbedComponent struct {\n}\nfunc (c *EmbedComponent) Calcium() string {\nreturn Calcium(\n)\n}\nfunc (c *EmbedComponent) CalciumBuffer() *bytes.Buffer {\nreturn embedded_1.CalciumBuffer(\nc.Calcium(),\n)\n}\nfunc (c *EmbedComponent) BufferPrinter() examples.Printer {\nreturn embedded_1.BufferPrinter(\nc.CalciumBuffer(),\n)\n}\n",
			},
		}, {
			Name: "garbage",
			Given: given{
				Source: "../../examples/garbage/garbage_module.go",
			},
			Expected: expected{
				Error: ErrSourceIsInvalid,
			},
		}, {
			Name: "generics",
			Given: given{
				Source: "../../examples/generics/generics.go",
			},
			Expected: expected{
				src: "package generics\n\nimport (\n\"gitlab.com/cosban/di/examples\"\n)\nvar singleton *GenericsComponent\nfunc GetGenericsComponent() *GenericsComponent {\nif singleton == nil {\nsingleton = &GenericsComponent{}\n}\nreturn singleton\n}\ntype GenericsComponent struct {\n}\nfunc (c *GenericsComponent) Name() string {\nreturn Name(\n)\n}\nfunc (c *GenericsComponent) GenericPrinter() examples.GenericPrinter[int] {\nreturn GenericPrinter(\nc.Name(),\n)\n}\nfunc (c *GenericsComponent) PrinterEvaluator() examples.PrinterEvaluator[int] {\nreturn PrinterEvaluator(\nc.GenericPrinter(),\n)\n}\n",
			},
		}, {
			Name: "missing",
			Given: given{
				Source: "../../examples/missing/missing.go",
			},
			Expected: expected{
				Error: ErrMissingDependency,
			},
		}, {
			Name: "loose",
			Given: given{
				Source: "../../examples/loose/loose_module.go",
			},
			Expected: expected{
				Error: ErrCyclicalDependency,
			},
		}, {
			Name: "simple",
			Given: given{
				Source: "../../examples/simple/simple_module.go",
			},
			Expected: expected{
				src: "package simple\n\nimport (\n\"bytes\"\n\"fmt\"\n\"gitlab.com/cosban/di/examples\"\n)\nvar singleton *SimpleComponent\nfunc GetSimpleComponent() *SimpleComponent {\nif singleton == nil {\nsingleton = &SimpleComponent{}\n}\nreturn singleton\n}\ntype SimpleComponent struct {\n}\nfunc (c *SimpleComponent) Calcium() string {\nreturn Calcium(\n)\n}\nfunc (c *SimpleComponent) CalciumBuffer() *bytes.Buffer {\nreturn CalciumBuffer(\nc.Calcium(),\n)\n}\nfunc (c *SimpleComponent) BufferPrinter() examples.Printer {\nreturn BufferPrinter(\nc.CalciumBuffer(),\n)\n}\n",
			},
		},
		{
			Name: "singletons",
			Given: given{
				Source: "../../examples/singletons/singletons_module.go",
			},
			Expected: expected{
				src: "package singletons\n\nimport (\n\"bytes\"\n\"gitlab.com/cosban/di/examples\"\n)\nvar singleton *SingletonsComponent\nfunc GetSingletonsComponent() *SingletonsComponent {\nif singleton == nil {\nsingleton = &SingletonsComponent{}\n}\nreturn singleton\n}\ntype SingletonsComponent struct {\nbuffer **bytes.Buffer\nprinter *examples.Printer\n}\nfunc (c *SingletonsComponent) Buffer() *bytes.Buffer {\nif c.buffer == nil {\ntemp := Buffer(\n)\nc.buffer = &temp\n}\nreturn *c.buffer\n}\nfunc (c *SingletonsComponent) Printer() examples.Printer {\nif c.printer == nil {\ntemp := Printer(\nc.Buffer(),\n)\nc.printer = &temp\n}\nreturn *c.printer\n}\n",
			},
		}, {
			Name: "tight",
			Given: given{
				Source: "../../examples/tight/tight_module.go",
			},
			Expected: expected{
				Error: ErrCyclicalDependency,
			},
		},
	}

	for _, scenario := range scenarios {
		scenario := scenario
		t.Run(scenario.Name, func(t *testing.T) {
			src, err := (&Generator{}).Generate(scenario.Given.Source, fmt.Sprintf("di generate --source=%s", scenario.Given.Source))
			assert.ErrorIs(t, err, scenario.Expected.Error)
			if scenario.Expected.Error == nil {
				header := fmt.Sprintf(""+
					"// Code generated by di; DO NOT EDIT.\n"+
					"//   Source: %s\n"+
					"//   Command: di generate --source=%s\n"+
					"//   Version: gitlab.com/cosban/di/v2/cmd/di (devel)\n",
					scenario.Given.Source,
					scenario.Given.Source,
				)

				assert.Equal(t, header+scenario.Expected.src, string(src))
			}
		})
	}
}
