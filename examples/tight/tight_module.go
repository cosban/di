//go:generate di generate --output=tight_module.gen.go --source=tight_module.go
package tight

import "gitlab.com/cosban/di/examples"

func PrerequisitePrinterA(prerequisitePrinterB examples.Printer) examples.Printer {
	return nil
}

func PrerequisitePrinterB(prerequisitePrinterA examples.Printer) examples.Printer {
	return nil
}
