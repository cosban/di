package di

import (
	"gitlab.com/cosban/di/v2/pkg/model"
)

// treeNode godoc
type treeNode struct {
	model.Identifier
	dependencies map[model.Identifier]treeNode
	prerequisite []model.Identifier
}

func newTreeNode(ident model.Identifier, requires ...model.Identifier) treeNode {
	return treeNode{
		Identifier:   ident,
		dependencies: map[model.Identifier]treeNode{},
		prerequisite: append([]model.Identifier{}, requires...),
	}
}

func toTree(providers []model.Provider) treeNode {
	remaining := map[model.Identifier]model.Provider{}
	for _, provider := range providers {
		remaining[provider.Identifier] = provider
	}
	root := newTreeNode(model.Identifier{})
	completed := map[model.Identifier]treeNode{root.Identifier: root}
	return buildTree(remaining, root, completed)
}

func buildTree(
	providers map[model.Identifier]model.Provider,
	root treeNode,
	completed map[model.Identifier]treeNode,
) treeNode {
	if len(providers) == 0 {
		return root
	}
	remaining := map[model.Identifier]model.Provider{}
	for ident, provider := range providers {
		node := newTreeNode(ident, provider.Requirements...)
		if len(provider.Requirements) == 0 {
			root.dependencies[ident] = node
		}
		for _, required := range provider.Requirements {
			if _, ok := completed[required]; ok {
				completed[required].dependencies[node.Identifier] = node
			} else {
				remaining[ident] = provider
				break
			}
		}
		if _, ok := remaining[ident]; !ok {
			completed[ident] = node
		}
	}
	return buildTree(remaining, root, completed)
}
