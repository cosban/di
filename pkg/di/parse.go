package di

import (
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"strings"

	"golang.org/x/tools/go/packages"

	"gitlab.com/cosban/di/v2/pkg/model"
)

var ErrSourceIsInvalid = fmt.Errorf("source is invalid")

// parse parses the given source file and returns a model.File.
func parse(source string) (*model.File, error) {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, source, nil, parser.ParseComments)
	if err != nil {
		return nil, errors.Join(ErrSourceIsInvalid, err)
	}
	return toModelFile(f, source)
}

// toModelFile converts an ast.File to a model.File by walking the ast and extracting the necessary information.
func toModelFile(astFile *ast.File, source string) (*model.File, error) {
	var err error
	modelFile := model.NewFile(source, astFile.Name.String())
	ast.Inspect(astFile, func(node ast.Node) bool {
		_func, ok := node.(*ast.FuncDecl)
		if ok && _func != nil {
			var provider *model.Provider
			provider, err = parseFunction(_func)
			if err != nil {
				return true
			}
			if provider.IsComponent {
				if _, ok := modelFile.Components[provider.Identifier]; ok {
					err = fmt.Errorf("duplicate component identifier received: %s", provider.Identifier)
					return true
				}
				modelFile.Components[provider.Identifier] = model.NewEmbeddedComponent(provider.Identifier)
			} else {
				modelFile.Providers.Add(*provider)
			}
			return false
		}
		_import, ok := node.(*ast.ImportSpec)
		if ok && _import != nil {
			path, proceed := parseImport(_import)
			modelFile.Imports = append(modelFile.Imports, path)
			return proceed
		}
		return true
	})
	return modelFile, err
}

var (
	ErrMustReturnSomething   = fmt.Errorf("providers must return something")
	ErrMustNotReturnTuple    = fmt.Errorf("providers must not return tuples")
	ErrUnableToParseFuncBody = fmt.Errorf("provider function has no parsable body")
)

// parseFunction parses a function and returns a model.Provider.
func parseFunction(_func *ast.FuncDecl) (*model.Provider, error) {
	if _func.Body == nil {
		return nil, fmt.Errorf("%w (name: %s)", ErrUnableToParseFuncBody, _func.Name.String())
	}
	// no receivers allowed because they would need to have already been created
	if _func.Recv != nil {
		return nil, nil
	}
	if len(_func.Type.Results.List) == 0 {
		return nil, ErrMustReturnSomething
	}
	// providers must provide only one thing
	if len(_func.Type.Results.List) != 1 {
		return nil, ErrMustNotReturnTuple
	}
	provider := model.Provider{
		Identifier:   model.Identifier{},
		Requirements: make([]model.Identifier, 0),
		IsSingleton:  false,
		IsComponent:  false,
	}

	docs := strings.Split(_func.Doc.Text(), "\n")
	for _, line := range docs {
		switch strings.TrimSpace(line) {
		case "@singleton":
			provider.IsSingleton = true
		case "@component":
			provider.IsComponent = true
		}
	}

	for _, result := range _func.Type.Results.List {
		var err error
		provider.Identifier, err = model.DetermineIdentifier(_func.Name.String(), result.Type)
		if err != nil {
			panic(err)
		}
	}
	for _, param := range _func.Type.Params.List {
		requirement, err := model.IdentifierFromField(param)
		if err != nil {
			panic(err)
		}
		provider.Requirements = append(provider.Requirements, requirement)

	}

	return &provider, nil
}

// parseImport parses an import and returns the import path.
func parseImport(_import *ast.ImportSpec) (string, bool) {
	var path string
	if _import.Name == nil {
		path = _import.Path.Value
	} else {
		path = fmt.Sprintf("%s %s", _import.Name, _import.Path.Value)
	}
	return path, false
}

// parseEmbeds parses all embedded components in the given file.
func parseEmbeds(file *model.File) error {
	for _, component := range file.Components {
		if err := parseEmbed(component, file); err != nil {
			return err
		}
	}
	return nil
}

// parseEmbed parses an embedded component.
func parseEmbed(component *model.EmbeddedComponent, file *model.File) error {
	if !strings.HasPrefix(component.Type, "*") {
		return fmt.Errorf("embedded component providers must return a pointer (provider: %s)", component.Identifier)
	}

	// part 1: find the correct import packages
	var err error
	component.ImportPath, err = getImportPathFor(component.Identifier, file)
	if err != nil {
		return err
	}
	// part 2: load that package
	pkg, err := loadImportPath(component.ImportPath)
	if err != nil {
		return err
	}
	// part 3: inspect that package
	// we want to find all functions which are parts of the component and then find their counterpart definitions.
	// the good news is that all the necessary functions are exported.
	return parseEmbeddedPackage(component, pkg)
}

// parseEmbeddedPackage parses the entirety of an embedded package. It determines which functions are part of the
// embedded component, then looks for the counterpart provider definitions. Imports are also collected.
func parseEmbeddedPackage(component *model.EmbeddedComponent, pkg *packages.Package) error {
	// ok the bad news is that we don't know the order... so we need to figure that out first.
	componentFile, otherFiles := determineParseOrdering(pkg)
	if componentFile == nil {
		return fmt.Errorf("unable to find component file for embedded component")
	}

	component.Functions = getComponentFuncs(component, componentFile)

	for _, file := range otherFiles {
		ast.Inspect(file, func(node ast.Node) bool {
			_func, ok := node.(*ast.FuncDecl)
			if ok && _func != nil {
				provider, err := parseFunction(_func)
				if err != nil {
					return true
				}
				if _, ok := component.Functions[provider.Identifier]; ok {
					component.Providers.Add(*provider)
				}
			}
			_import, ok := node.(*ast.ImportSpec)
			if ok && _import != nil {
				path, proceed := parseImport(_import)
				component.Imports = append(component.Imports, path)
				return proceed
			}
			return true
		})
	}
	return nil
}

func determineParseOrdering(pkg *packages.Package) (*ast.File, []*ast.File) {
	for i, file := range pkg.Syntax {
		// TODO: should I be checking for multiple modules? It would indicate a bad state, but could be more user friendly.
		if len(file.Comments) > 0 && strings.HasPrefix(file.Comments[0].Text(), "Code generated by di; DO NOT EDIT.") {
			return file, append(pkg.Syntax[:i], pkg.Syntax[i+1:]...)
		}
	}
	return nil, nil
}

func getComponentFuncs(component *model.EmbeddedComponent, file *ast.File) map[model.Identifier]struct{} {
	funcs := map[model.Identifier]struct{}{}
	ast.Inspect(file, func(node ast.Node) bool {
		_func, ok := node.(*ast.FuncDecl)
		if ok && _func != nil {
			if _func.Recv == nil || len(_func.Recv.List) != 1 {
				return true
			}
			receiver := _func.Recv.List[0]
			var receiverType string
			if expr, ok := receiver.Type.(*ast.StarExpr); ok {
				if ident, ok := expr.X.(*ast.Ident); ok {
					receiverType = fmt.Sprintf("*%s.%s", file.Name, ident.Name)
				}
			} else {
				return true
			}

			if receiverType != component.Type {
				return true
			}
			if len(_func.Type.Results.List) != 1 {
				return true
			}
			identifier, err := model.DetermineIdentifier(_func.Name.String(), _func.Type.Results.List[0].Type)
			if err != nil {
				return true
			}
			funcs[identifier] = struct{}{}
		}
		return true
	})
	return funcs
}

func loadImportPath(importPath string) (*packages.Package, error) {
	pkgs, err := packages.Load(&packages.Config{
		Mode:  packages.NeedDeps | packages.NeedName | packages.NeedFiles | packages.NeedImports | packages.NeedSyntax,
		Tests: false,
	}, importPath)
	if err != nil {
		return nil, fmt.Errorf("failed to load package: %w", err)
	}

	if len(pkgs) == 0 {
		return nil, fmt.Errorf("unable to find package for %s", importPath)
	}
	if len(pkgs) > 1 {
		// this should never happen
		return nil, fmt.Errorf("found multiple packages for %s", importPath)
	}
	return pkgs[0], nil
}

func getImportPathFor(identifier model.Identifier, file *model.File) (string, error) {
	packageIndicator := strings.Index(identifier.Type, ".")
	if packageIndicator == -1 {
		return "", fmt.Errorf("component is not imported from another package (%s)", identifier)
	}
	for _, _import := range file.Imports {
		if isImportFor(identifier.Type[1:packageIndicator], _import) {
			return strings.Trim(_import, "\""), nil
		}
	}
	return "", fmt.Errorf("unable to find import for %s", identifier)
}

func isImportFor(componentPackage, _import string) bool {
	if len(_import) == 0 {
		return false
	}
	var packageName string
	if strings.Contains(_import, " ") {
		// import is aliased
		packageName = strings.Split(_import, " ")[0]
	} else {
		// import is not aliased
		lastSlash := strings.LastIndex(_import, "/")
		packageName = _import[lastSlash+1 : len(_import)-1]
	}
	return componentPackage == packageName
}
