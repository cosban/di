package model

import (
	"go/ast"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIdentifier_ErrorString(t *testing.T) {
	type given struct {
		Name string
		Type string
	}
	type expected struct {
		ErrorString string
	}
	for _, scenario := range []struct {
		Name     string
		Given    given
		Expected expected
	}{
		{
			Name: "empty",
			Given: given{
				Name: "",
				Type: "",
			},
			Expected: expected{
				ErrorString: "{no name given}() {no type given}",
			},
		}, {
			Name: "filled",
			Given: given{
				Name: "name",
				Type: "type",
			},
			Expected: expected{
				ErrorString: "name() type",
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			actual := Identifier{
				Name: scenario.Given.Name,
				Type: scenario.Given.Type,
			}
			assert.Equal(t, scenario.Expected.ErrorString, actual.ErrorString())
		})
	}
}

func TestNewIdentifier(t *testing.T) {
	type given struct {
		Name string
		Expr ast.Expr
	}
	type expected struct {
		Identifier Identifier
		Err        error
	}
	for _, scenario := range []struct {
		Name     string
		Given    given
		Expected expected
	}{
		{
			Name: "names are exported",
			Given: given{
				Name: "name",
				Expr: &ast.Ident{
					Name: "string",
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "string",
				},
			},
		}, {
			Name: "imported",
			Given: given{
				Name: "foo",
				Expr: &ast.SelectorExpr{
					X: &ast.Ident{
						Name: "pkg",
					},
					Sel: &ast.Ident{
						Name: "Struct",
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Foo",
					Type: "pkg.Struct",
				},
			},
		}, {
			Name: "pointers",
			Given: given{
				Name: "name",
				Expr: &ast.StarExpr{
					X: &ast.Ident{
						Name: "string",
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "*string",
				},
			},
		}, {
			Name: "slice",
			Given: given{
				Name: "name",
				Expr: &ast.ArrayType{
					Elt: &ast.Ident{
						Name: "int",
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "[]int",
				},
			},
		}, {
			Name: "map",
			Given: given{
				Name: "name",
				Expr: &ast.MapType{
					Key: &ast.Ident{
						Name: "string",
					},
					Value: &ast.Ident{
						Name: "int",
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "map[string]int",
				},
			},
		}, {
			Name: "generic struct",
			Given: given{
				Name: "name",
				Expr: &ast.IndexExpr{
					X: &ast.Ident{
						Name: "Struct",
					},
					Index: &ast.Ident{
						Name: "T",
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "Struct[T]",
				},
			},
		}, {
			Name: "multiple generic types",
			Given: given{
				Name: "name",
				Expr: &ast.IndexListExpr{
					X: &ast.Ident{
						Name: "Struct",
					},
					Indices: []ast.Expr{
						&ast.Ident{
							Name: "T",
						},
						&ast.Ident{
							Name: "U",
						},
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "Struct[T, U]",
				},
			},
		}, {
			Name: "pointer to map of multi-generic with slice of pointer to imported generic struct",
			Given: given{
				Name: "name",
				Expr: &ast.StarExpr{
					X: &ast.MapType{
						Key: &ast.IndexListExpr{
							X: &ast.Ident{
								Name: "Struct",
							},
							Indices: []ast.Expr{
								&ast.Ident{Name: "T"},
								&ast.Ident{Name: "U"},
							},
						},
						Value: &ast.StarExpr{
							X: &ast.ArrayType{
								Elt: &ast.IndexExpr{
									X: &ast.SelectorExpr{
										X: &ast.Ident{
											Name: "pkg",
										},
										Sel: &ast.Ident{
											Name: "Struct",
										},
									},
									Index: &ast.Ident{
										Name: "T",
									},
								},
							},
						},
					},
				},
			},
			Expected: expected{
				Identifier: Identifier{
					Name: "Name",
					Type: "*map[Struct[T, U]]*[]pkg.Struct[T]", // you monster
				},
			},
		},
	} {
		t.Run(scenario.Name, func(t *testing.T) {
			actual, err := DetermineIdentifier(scenario.Given.Name, scenario.Given.Expr)
			assert.ErrorIs(t, err, scenario.Expected.Err)
			assert.Equal(t, scenario.Expected.Identifier, actual)
		})
	}
}
