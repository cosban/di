package main

import (
	"os"

	"gitlab.com/cosban/di/v2/internal/cmd"
)

func main() {
	os.Exit(cmd.Do(os.Args[1:], os.Stdin, os.Stdout, os.Stderr))
}
