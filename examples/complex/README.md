# Complex Example

This example will generate a working component file. It shows that you may use more complex module set up where there are providers which require multiple other providers as well as providers which require providers that, themselves, require other providers.