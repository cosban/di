# Singletons Example

This example will generate a working component file. It shows that if `// @singleton` is specified above a provider, 
then the Generated module will treat it as a singleton.