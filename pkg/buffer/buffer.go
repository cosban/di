package buffer

import (
	"bytes"
	"fmt"
)

type FormatBuffer struct {
	bytes.Buffer
}

func (b *FormatBuffer) Write(template string, args ...any) (int, error) {
	return b.WriteString(fmt.Sprintf(template+"\n", args...))
}
