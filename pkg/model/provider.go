package model

import (
	"errors"
	"fmt"
	"go/ast"
	"strings"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

// Provider represents a function used to provide a dependency.
type Provider struct {
	Identifier
	Package      string
	IsSingleton  bool
	IsComponent  bool
	Requirements []Identifier
}

func (p *Provider) WithRequirement(name, kind string) *Provider {
	p.Requirements = append(p.Requirements, Identifier{Name: name, Type: kind})
	return p
}

// Identifier represents a dependency, including its name and type.
type Identifier struct {
	Name string
	Type string
}

func (i Identifier) String() string {
	name, typ := normalizeIdentParts(i)
	return fmt.Sprintf("%s %s", name, typ)
}

func (i Identifier) ErrorString() string {
	name, typ := normalizeIdentParts(i)
	return fmt.Sprintf("%s() %s", name, typ)
}

func normalizeIdentParts(i Identifier) (string, string) {
	name := i.Name
	if name == "" {
		name = "{no name given}"
	}
	typ := i.Type
	if typ == "" {
		typ = "{no type given}"
	}
	return name, typ
}

var caser = cases.Title(language.Und, cases.NoLower)

func IdentifierFromField(field *ast.Field) (Identifier, error) {
	return DetermineIdentifier(field.Names[0].String(), field.Type)
}

func DetermineIdentifier(name string, typ ast.Expr) (Identifier, error) {
	kind, err := getKind(typ)
	if err != nil {
		return Identifier{}, err
	}
	return Identifier{
		Name: caser.String(name),
		Type: kind,
	}, nil
}

func getKind(expr ast.Expr) (string, error) {
	var kind string
	var err error
	switch v := expr.(type) {
	case *ast.Ident:
		return v.String(), nil
	case *ast.SelectorExpr:
		innerKind, innerErr := getKind(v.X)
		kind = innerKind + "." + v.Sel.String()
		err = innerErr
	case *ast.StarExpr:
		innerKind, innerErr := getKind(v.X)
		kind = "*" + innerKind
		err = innerErr
	case *ast.ArrayType:
		innerKind, innerErr := getKind(v.Elt)
		kind = "[]" + innerKind
		err = innerErr
	case *ast.MapType:
		keyKind, keyErr := getKind(v.Key)
		valueKind, valueErr := getKind(v.Value)
		kind = "map[" + keyKind + "]" + valueKind
		err = errors.Join(keyErr, valueErr)
	case *ast.IndexExpr:
		innerKind, innerErr := getKind(v.X)
		indexKind, indexErr := getKind(v.Index)
		kind = innerKind + "[" + indexKind + "]"
		err = errors.Join(innerErr, indexErr)
	case *ast.IndexListExpr:
		innerKind, innerErr := getKind(v.X)
		indexKinds := make([]string, len(v.Indices))
		var indexErrs error
		for i, exp := range v.Indices {
			var indexErr error
			indexKinds[i], indexErr = getKind(exp)
			indexErrs = errors.Join(indexErrs, indexErr)
		}
		kind = innerKind + "[" + strings.Join(indexKinds, ", ") + "]"
		err = errors.Join(innerErr, indexErrs)
	default:
		return "", fmt.Errorf("could not discern type of expression at offset: %d", expr.Pos())
	}
	return kind, err
}
