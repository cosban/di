//go:build garbage

//go:generate di generate --output=garbage_module.gen.go --source=garbage_module.go
package garbage

import "fmt"

func Calcium( string {
	return fmt.Sprintf("%s", "calcium")
