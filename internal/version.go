package internal

import (
	"fmt"
	"runtime/debug"
)

func GetVersion() string {
	buildInfo, ok := debug.ReadBuildInfo()
	if !ok || buildInfo.Main == (debug.Module{}) {
		return "gitlab.com/cosban/di/v2/cmd/di (devel)"
	}
	return fmt.Sprintf("%s/cmd/di %s", buildInfo.Main.Path, buildInfo.Main.Version)
}
