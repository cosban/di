package di

import (
	"bytes"
	"fmt"

	"gitlab.com/cosban/di/v2/pkg/model"
)

var (
	ErrMissingDependency  = fmt.Errorf("missing dependency detected")
	ErrCyclicalDependency = fmt.Errorf("circular dependency detected")
)

// MissingError is used to denote that there are dependencies which were requested, but not provided within a module.
func MissingError(provider model.Provider, missing ...model.Identifier) error {
	fmt.Errorf("%w: %s %s", ErrMissingDependency, provider.Name, provider.Type)

	buffer := bytes.NewBufferString(fmt.Sprintf("%s depends on missing provider(s) with the following identifiers: ", provider.ErrorString()))
	prefix := ""
	for _, ident := range missing {
		buffer.WriteString(prefix)
		buffer.WriteString(ident.ErrorString())
		prefix = " and "
	}
	return fmt.Errorf("%w: %s", ErrMissingDependency, buffer.String())
}

// CyclicalError is used to denote that there are cyclical dependencies within a module.
func CyclicalError(first, second model.Identifier) error {
	cycles := map[model.Identifier]model.Identifier{
		first:  second,
		second: first,
	}
	var buffer bytes.Buffer
	prefix := ""
	for first, second := range cycles {
		buffer.WriteString(prefix)
		buffer.WriteString(first.ErrorString())
		buffer.WriteString(" <-> ")
		buffer.WriteString(second.ErrorString())
		prefix = " and "
	}
	return fmt.Errorf("%w: %s", ErrCyclicalDependency, buffer.String())
}

func validateDependencyTree(providers []model.Provider) error {
	mapped := map[model.Identifier]model.Provider{}
	for _, provider := range providers {
		mapped[provider.Identifier] = provider
	}
	if err := detectMissingDependencies(mapped); err != nil {
		return err
	}
	if err := detectCircularDependencies(mapped); err != nil {
		return err
	}
	return nil
}

func detectMissingDependencies(providers map[model.Identifier]model.Provider) error {
	for _, provider := range providers {
		for _, prereq := range provider.Requirements {
			if _, ok := providers[prereq]; !ok {
				return MissingError(provider, prereq)
			}
		}
	}
	return nil
}

func detectCircularDependencies(mapped map[model.Identifier]model.Provider) error {
	for ident := range mapped {
		if cycle := hasCircularDeps(
			mapped[ident],
			mapped[ident],
			mapped,
			map[model.Identifier]bool{},
		); cycle != nil {
			return cycle
		}
	}
	return nil
}

func hasCircularDeps(
	root, current model.Provider,
	providers map[model.Identifier]model.Provider,
	visited map[model.Identifier]bool,
) error {
	for _, dependency := range current.Requirements {
		if root.Identifier == dependency {
			return CyclicalError(root.Identifier, current.Identifier)
		}
		if ok := visited[dependency]; !ok {
			visited[dependency] = true
			if found := hasCircularDeps(
				root,
				providers[dependency],
				providers,
				visited,
			); found != nil {
				return found
			}
		}
	}
	return nil
}
