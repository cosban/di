package di

import (
	"testing"
)

func Test_isImportFor(t *testing.T) {
	type args struct {
		componentPackage string
		_import          string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "non-aliased",
			args: args{
				componentPackage: "simple",
				_import:          "\"github.com/cosban/di/v2/examples/simple\"",
			},
			want: true,
		}, {
			name: "aliased",
			args: args{
				componentPackage: "blarg",
				_import:          "blarg \"github.com/cosban/di/v2/examples/simple\"",
			},
			want: true,
		}, {
			name: "non-matching non-alias",
			args: args{
				componentPackage: "blarg",
				_import:          "\"github.com/cosban/di/v2/examples/simple\"",
			},
			want: false,
		}, {
			name: "non-matching alias",
			args: args{
				componentPackage: "simple",
				_import:          "blarg \"github.com/cosban/di/v2/examples/simple\"",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isImportFor(tt.args.componentPackage, tt.args._import); got != tt.want {
				t.Errorf("isImportFor() = %v, want %v", got, tt.want)
			}
		})
	}
}
