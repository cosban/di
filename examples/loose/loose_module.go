//go:generate di generate --output=loose_module.gen.go --source=loose_module.go
package loose

import "gitlab.com/cosban/di/examples"

func PrerequisitePrinterA(prerequisitePrinterB examples.Printer) examples.Printer {
	return nil
}

func PrerequisitePrinterB(prerequisitePrinterC examples.Printer) examples.Printer {
	return nil
}

func PrerequisitePrinterC(prerequisitePrinterA examples.Printer) examples.Printer {
	return nil
}
