package cmd

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"golang.org/x/tools/imports"

	"gitlab.com/cosban/di/v2/internal"
	"gitlab.com/cosban/di/v2/pkg/di"
)

func Do(args []string, stdin io.Reader, stdout io.Writer, stderr io.Writer) int {
	root := &cobra.Command{Use: "di", SilenceUsage: true, SilenceErrors: true}

	root.AddCommand(versionCommand())
	root.AddCommand(generateCommand())

	root.SetArgs(args)
	root.SetIn(stdin)
	root.SetOut(stdout)
	root.SetErr(stderr)

	ctx := context.Background()
	if err := root.ExecuteContext(ctx); err != nil {
		fmt.Fprintf(root.ErrOrStderr(), "di error: %s\n", err.Error())
		return 1
	}

	return 0
}

func versionCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Print the di version number",
		RunE: func(cmd *cobra.Command, args []string) error {
			version := internal.GetVersion()
			fmt.Fprintln(cmd.OutOrStdout(), version)
			return nil
		},
	}
}

func generateCommand() *cobra.Command {
	var source string
	var output string

	command := &cobra.Command{
		Use:   "generate",
		Short: "Generate a Dependency Injection component from a provider file.",
		RunE: func(cmd *cobra.Command, args []string) error {
			if source == "" {
				return fmt.Errorf("no source file was provided")
			}
			generator := &di.Generator{}
			src, err := generator.Generate(source, "di "+strings.Join(os.Args[1:], " "))
			if err != nil {
				return err
			}
			formatted, err := imports.Process("", src, &imports.Options{
				AllErrors: true,
				Comments:  true,
				TabIndent: true,
				TabWidth:  8,
			})
			if err != nil {
				return fmt.Errorf("formatting error: %w\n\n===CODE BEGIN===\n%s\n===CODE END===", err, src)
			}
			if output == "" {
				fmt.Fprintf(cmd.OutOrStdout(), "%s", formatted)
				return nil
			}
			location := filepath.Join(filepath.Dir("."), output)
			os.WriteFile(location, formatted, 0644)
			return nil
		},
	}
	command.Flags().StringVarP(&source, "source", "", "", "The source file to generate a component from.")
	command.Flags().StringVarP(&output, "output", "", "", "The desired name of the output file within this directory. If not provided, output will be printed to stdout.")

	return command
}
